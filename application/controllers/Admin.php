<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends TG_Controller
{
    function __construct()
    {
        parent::__construct();
        // $this->load->model('user_model');
        // $this->user = $this->session->userdata('user');
        // is_logged_in();

        // if (!$this->session->userdata('email')) {
        //     redirect('auth');
        // }
    }

    function index()
    {
        $data['title'] = 'Dashboard';
        $data['view'] = 'admin/index';
        $this->display($data);

        // $queryMenu = "SELECT a.id, a.menu 
        //                 FROM user_menu a JOIN user_access_menu b ON a.id = b.menu_id
        //                WHERE b.role_id = $this->user['role_id'] ORDER BY b.menu_id ASC";

        // $data['menu'] = $this->db->query($queryMenu)->result_array();

        // $querySubMenu = "SELECT * FROM user_sub_menu
        //                   WHERE menu_id = {$m['id']}
        //                     AND is_active = 1";

        // $subMenu = $this->db->query($querySubMenu)->result_array();

        
        // $data['title'] = 'Dashboard';
        // // $data['user'] = $this->user_model->get_user();
        // $data['user'] = $this->user; //$this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        // $this->load->view('templates/header', $data);
        // $this->load->view('templates/sidebar', $data);
        // $this->load->view('templates/topbar', $data);
        // $this->load->view('admin/index', $data);
        // $this->load->view('templates/footer');
    }

    // function add_new(){
    //   $this->load->view('add_user');
    // }

    // function save(){
    //   $username = $this->input->post('username');
    //   $nama = $this->input->post('nama');
    //   $alamat = $this->input->post('alamat');
    //   $no_hp = $this->input->post('no_hp');
    //   $this->user_model->save($username,$nama,$alamat,$no_hp);
    //   redirect('user');
    // }

    // function delete(){
    //   $id = $this->uri->segment(3);
    //   $this->user_model->delete($id);
    //   redirect('user');
    // }

    // function get_edit(){
    //   $id = $this->uri->segment(3);
    //   $result = $this->user_model->get_id($id);
    //   if($result->num_rows() > 0){
    //       $i = $result->row_array();
    //       $data = array(
    //           'id'        => $i['id'],
    //           'username'  => $i['username'],
    //           'nama'      => $i['nama'],
    //           'alamat'    => $i['alamat'],
    //           'no_hp'     => $i['no_hp']
    //       );
    //       $this->load->view('edit_user',$data);
    //   }else{
    //       echo "Data Was Not Found";
    //   }
    // }

    // function update(){
    //   $id = $this->input->post('id');
    //   $username = $this->input->post('username');
    //   $nama = $this->input->post('nama');
    //   $alamat = $this->input->post('alamat');
    //   $no_hp = $this->input->post('no_hp');
    //   $this->user_model->update($id,$username,$nama,$alamat,$no_hp);
    //   redirect('user');
    // }
}
