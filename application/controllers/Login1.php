<?php
class Login extends CI_Controller
{
	public function __construct(){
        parent::__construct();
        $this->load->model('user_model');
        $this->table = 'user';
    }

    public function index(){
		//untuk menampilkan halaman view dengan nama login
		$this->load->view('login');
	}

    public function process(){
        //untuk cek apakah tombol login diklik
        if(isset($_POST['login']))
        {
            //mengambil data username dan password dari inputan pengguna
            $username = $this->input->post('username');
            $password = $this->input->post('password');

            //untuk cek apakah username dan password telah terisi
            if($username and $password)
            {
                $data = [
                        'username' => $username,
                        'password' => $password
                        ];
                $user_account = $this->user_model->get_user($this->table, $data)->row();

                if($user_account)
                {
                    $session_data = [
                                'username' => $user_account->username,
                                'nama' => $user_account->nama
                                ];
                    $this->session->set_userdata($session_data);
                    redirect('user');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Username atau password tidak cocok');
                    redirect('login');
                }
            }
            else
            {
                $this->session->set_flashdata('error', 'Seluruh data harus diisi');
                    redirect('login');
            }
        }
    }
}