<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Menu extends TG_Controller
{
    function __construct()
    {
        parent::__construct();
        // $this->load->model('User_model');
        $this->load->model('Menu_model', 'menu');
        $this->load->model('SubMenu_model', 'submenu');
        is_logged_in();
        // if (!$this->session->userdata('email')) {
        //     redirect('auth');
        // }
    }

    function index()
    {
        $data['title'] = 'Menu Management';
        // $data['user'] = $this->user_model->get_user();
        // $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        // $data['menu'] = $this->db->get('user_menu')->result_array();
        $data['menu'] = $this->menu->get();

        $this->form_validation->set_rules('menu', 'Menu name', 'required');

        if ($this->form_validation->run() == false) {

            $data['view'] = 'menu/index';
            $this->display($data);
            // $this->load->view('templates/header', $data);
            // $this->load->view('templates/sidebar', $data);
            // $this->load->view('templates/topbar', $data);
            // $this->load->view('menu/index', $data);
            // $this->load->view('templates/footer');
        } else {
            // $this->db->insert('user_menu', ['menu' => $this->input->post('menu')]);
            $data = ['menu' => $this->input->post('menu')];
            $this->menu->insert($data);
            
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                    New menu added!</div>');
            redirect('menu');
        }
    }

    public function submenu()
    {
        $data['title'] = 'Submenu Management';
        // $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        // $data['menu'] = $this->db->get('user_menu')->result_array();
        $data['menu'] = $this->menu->get();

        $data['subMenu'] = $this->menu->getSubMenu();

        // print_r($data['subMenu']); die();
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('menu_id', 'Menu name', 'required');
        $this->form_validation->set_rules('url', 'URL', 'required');
        $this->form_validation->set_rules('icon', 'Icon', 'required');

        if ($this->form_validation->run() == false) {

            $data['view'] = 'menu/submenu';
            $this->display($data);
            // $this->load->view('templates/header', $data);
            // $this->load->view('templates/sidebar', $data);
            // $this->load->view('templates/topbar', $data);
            // $this->load->view('menu/submenu', $data);
            // $this->load->view('templates/footer');
        } else {
            $data = [
                'title' => $this->input->post('title'),
                'menu_id' => $this->input->post('menu_id'),
                'url' => $this->input->post('url'),
                'icon' => $this->input->post('icon'),
                'is_active' => $this->input->post('is_active'),
                'created_by' => $this->user['username']
            ];
            // $this->db->insert('user_sub_menu', $data);
            $this->submenu->insert($data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                    New Submenu added!</div>');
            redirect('menu/submenu');
        }
    }
}
