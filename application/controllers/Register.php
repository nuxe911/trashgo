<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

   function __construct(){
       parent::__construct();
       $this->load->library(array('form_validation'));
       $this->load->helper(array('url','form'));
       $this->load->model('user_model'); //call model
   }

   public function index() {

       $this->form_validation->set_rules('nama', 'NAMA','required');
       $this->form_validation->set_rules('username', 'USERNAME','required');
       $this->form_validation->set_rules('password','PASSWORD','required');
       $this->form_validation->set_rules('password_conf','PASSWORD','required|matches[password]');
       $this->form_validation->set_rules('alamat','ALAMAT','required');
       $this->form_validation->set_rules('no_hp','NO_HP','required');
       if($this->form_validation->run() == FALSE) {
           $this->load->view('account/v_register');
       }else{

           $data['nama']   =    $this->input->post('nama');
           $data['username'] =    $this->input->post('username');
           $data['password'] =    md5($this->input->post('password'));
           $data['alamat']  =    $this->input->post('alamat');
           $data['no_hp']  =    $this->input->post('no_hp');
           
           $this->user_model->daftar($data);

           $pesan['message'] =    "Pendaftaran berhasil";

           $this->load->view('account/v_success',$pesan);
       }
   }
}