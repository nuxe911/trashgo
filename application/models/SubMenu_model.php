<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SubMenu_model extends CI_Model
{

    public function insert($data)
    {
        $this->db->insert('user_sub_menu', $data);
        return true;
    }

}
