<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_model extends CI_Model
{

    function get_user()
    {
        $result = $this->db->get('user');
        return $result;
    }

    function save($username, $nama, $alamat, $no_hp)
    {
        $data = array(
            'username' => $username,
            'nama' => $nama,
            'alamat' => $alamat,
            'no_hp' => $no_hp
        );
        $this->db->insert('user', $data);
    }

    function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('user');
    }

    function get_id($id)
    {
        $query = $this->db->get_where('user', array('id' => $id));
        return $query;
    }

    function update($id, $username, $nama, $alamat, $no_hp)
    {
        $data = array(
            'username' => $username,
            'nama'     => $nama,
            'alamat'   => $alamat,
            'no_hp'    => $no_hp
        );
        $this->db->where('id', $id);
        $this->db->update('user', $data);
    }

    function daftar($data)
    {
        $this->db->insert('user', $data);
    }
}
