<?php defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>  
<head>
<meta charset="UTF-8">
<title>
  Pendaftaran Akun | Tutorial Simple Login Register CodeIgniter @ http://recodeku.blogspot.com
</title>
</head>
<body>
  <h2>Pendaftaran Akun</h2>

  <?php echo form_open('register');?>
  <p>Nama:</p>
  <p>
    <input type="text" name="nama" value="<?php echo set_value('nama'); ?>"/>
  </p>
  <p> <?php echo form_error('nama'); ?> </p>

  <p>Username:</p>
  <p>
    <input type="text" name="username" value="<?php echo set_value('username'); ?>"/> 
  </p>
  <p> <?php echo form_error('username'); ?> </p>

  <p>Password:</p>
  <p>
    <input type="password" name="password" value="<?php echo set_value('password'); ?>"/>
  </p>
  <p> <?php echo form_error('password'); ?> </p>

  <p>Password Confirm:</p>
  <p>
    <input type="password" name="password_conf" value="<?php echo set_value('password_conf'); ?>"/>
  </p>
  <p> <?php echo form_error('password_conf'); ?> </p>

  <p>Alamat:</p>
  <p>
    <input type="text" name="alamat" value="<?php echo set_value('alamat'); ?>"/>
  </p>
  <p> <?php echo form_error('alamat'); ?> </p>

  <p>Nomor HP:</p>
  <p>
    <input type="text" name="no_hp" value="<?php echo set_value('no_hp'); ?>"/>
  </p>
  <p> <?php echo form_error('no_hp'); ?> </p>

  <p>
    <input type="submit" name="btnSubmit" value="Daftar" />
  </p>

  <?php echo form_close();?>

  <p>
  Kembali ke beranda, Silakan klik <?php echo anchor(site_url().'/beranda','di sini..'); ?>
  </p>
</body>
</html>