<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-recycle"></i>
        </div>
        <div class="sidebar-brand-text mx-3">trashgo</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <?php
    // $role_id = $this->session->userdata('role_id');
    // $queryMenu = "SELECT a.id, a.menu 
    //                 FROM user_menu a JOIN user_access_menu b ON a.id = b.menu_id
    //                WHERE b.role_id = $role_id ORDER BY b.menu_id ASC";

    // $menu = $this->db->query($queryMenu)->result_array();
    ?>

    <!-- LOOPING MENU DAN SUB MENU-->
    <?php foreach ($smenu as $m) : ?>
        <div class="sidebar-heading">
            <?= $m['menu']; ?>
        </div>

        <?php
        // $querySubMenu = "SELECT * FROM user_sub_menu
        //                   WHERE menu_id = {$m['id']}
        //                     AND is_active = 1";

        // $subMenu = $this->db->query($querySubMenu)->result_array();
        ?>

            <?php foreach ($ssubMenu as $sm) : ?>
                <?php if ($m['id'] == $sm['menu_id']) : ?>
                    <?php if ($title == $sm['title']) : ?>
                        <li class="nav-item active">
                        <?php else : ?>
                        <li class="nav-item">
                        <?php endif ?>
                        <a class="nav-link" href="<?= base_url($sm['url']); ?> ">
                            <i class="<?= $sm['icon']; ?>"></i>
                            <span><?= $sm['title']; ?></span></a>
                        </li>
                <?php endif ?>
            <?php endforeach; ?>

            <!-- Divider -->
            <hr class="sidebar-divider">
        <?php endforeach; ?>

        <li class="nav-item">
            <a class="nav-link" href="<?= base_url('auth/logout'); ?>">
                <i class="fas fa-fw fa-sign-out-alt"></i>
                <span>Logout</span></a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline">
            <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>

</ul>
<!-- End of Sidebar -->